<?php

namespace Drupal\chosen_autocomplete\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Field Settings form.
 */
class ChosenAutocompleteEntityReferenceItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'chosen_on' => 0,
      'chosen_placeholder' => \Drupal::translation()->translate('Choose an option'),
      'chosen_typing_placeholder' => \Drupal::translation()->translate('Keep typing...'),
      'chosen_searching_placeholder' => \Drupal::translation()->translate('Looking for'),
      'chosen_pattern' => '/(.+) \((\d+)\)/',
      'chosen_replacement' => '${1}',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    return parent::fieldSettingsForm($form, $form_state);
  }

}
