<?php

namespace Drupal\chosen_autocomplete\Plugin\Field\FieldWidget;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Field widget file for chosen auocomplete.
 *
 * @FieldWidget(
 *  id = "chosen_autocomplete",
 *  label = @Translation("Chosen Autocomplete"),
 *  field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class ChosenAutocomplete extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    // Basic widget setting.
    return [
      'size' => 60,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field Size'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Size: @size', ['@size' => $this->getSetting('size')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Original entity reference field has 2 more fields as addition.
    // Select field is used to allow chosen
    // library process as regular select field.
    $entity = $items->getEntity();
    $referenced_entities = $items->referencedEntities();
    $options = [];
    $default_options = [];
    $fieldDefinitions = $items->getFieldDefinition();
    $field_name = $fieldDefinitions->get('field_name');
    $select_id = 'edit-' . str_replace('_', '-', $field_name) . '-target-id-select';
    $if_autocomplete_id = 'edit-' . str_replace('_', '-', $field_name) . '-chosen-chosen-autocreate';
    // Use \Drupal\field\Entity\FieldConfig;.
    $form['#chosen_autocomplete'][] = $field_name;

    $element += [
      '#type' => 'entity_autocomplete',
      '#target_type' => $this->getFieldSetting('target_type'),
      '#selection_handler' => $this->getFieldSetting('handler'),
      '#selection_settings' => $this->getFieldSetting('handler_settings'),
      // Entity reference field items are handling validation themselves via
      // the 'ValidReference' constraint.
      '#validate_reference' => FALSE,
      '#maxlength' => 1024,
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#default_value' => $referenced_entities,
      '#attributes' => [
        'style' => 'display:none',
      ],
      '#tags' => TRUE,
    ];
    // Use Drupal\taxonomy\Entity\Term;.
    if (count($referenced_entities) > 0) {
      foreach ($referenced_entities as $entity_referenced) {
        $term_title = $entity_referenced->getName();
        $term_id = $entity_referenced->get('tid')->value;
        $options[$term_title . ' (' . $term_id . ')'] = $term_title;
      }
      $default_options = array_keys($options);
    }
    $helper['chosen_select'] = [
      '#type' => 'select',
      '#title' => '',
      '#validated' => TRUE,
      '#options' => $options,
      '#default_value' => $default_options,
      '#multiple' => TRUE,
      '#chosen' => TRUE,
      '#attributes' => [
        'id' => $select_id,
        'class' => ['form-chosen-ajax', 'chosen-select'],
      ],
      '#chosen_ajax' => [
        'options' => [
          'placeholder_text' => 'Choose an option',
          'typing_placeholder' => 'Keep typing...',
          'searching_placeholder' => 'Looking for',
        ],
      ],
      '#attached' => [
        'library' => [
          'chosen/drupal.chosen',
        ],
      ],
    ];
    $helper['chosen_hidden'] = [
      '#type' => 'hidden',
      '#value' => '',
      '#multiple' => TRUE,
      '#id' => '',
      '#attributes' => [
        'id' => $select_id . '-chosen-ajax',
        'class' => ['chosen-ajax'],
      ],
      '#disabled' => TRUE,
      '#attached' => [
        'library' => [
          'chosen_autocomplete/drupal.chosen_ajax',
        ],
      ],
    ];
    $autocreate = $this->getSelectionHandlerSetting('auto_create');
    $helper['chosen_autocreate'] = [
      '#type' => 'hidden',
      '#value' => (empty($autocreate)) ? 0 : 1,
      '#attributes' => [
        'id' => $if_autocomplete_id,
      ],
    ];
    if ($autocreate && ($bundle = $this->getAutocreateBundle())) {
      $element['#autocreate'] = [
        'bundle' => $bundle,
        'uid' => ($entity instanceof EntityOwnerInterface) ? $entity->getOwnerId() : \Drupal::currentUser()
          ->id(),
      ];
    }
    else {
      $element['#autocreate'] = FALSE;
    }

    return ['target_id' => $element, 'chosen' => $helper];
  }

  /**
   * Returns the value of a setting for the entity reference selection handler.
   *
   * @param string $setting_name
   *   The setting name.
   *
   * @return mixed
   *   The setting value.
   */
  protected function getSelectionHandlerSetting($setting_name) {
    $settings = $this->getFieldSetting('handler_settings');
    return isset($settings[$setting_name]) ? $settings[$setting_name] : NULL;
  }

  /**
   * Returns the name of the bundle which will be used for autocreated entities.
   *
   * @return string
   *   The bundle name.
   */
  protected function getAutocreateBundle() {
    $bundle = NULL;
    if ($this->getSelectionHandlerSetting('auto_create') && $target_bundles = $this->getSelectionHandlerSetting('target_bundles')) {
      // If there's only one target bundle, use it.
      if (count($target_bundles) == 1) {
        $bundle = reset($target_bundles);
      }
      // Otherwise use the target bundle stored in selection handler settings.
      elseif (!$bundle = $this->getSelectionHandlerSetting('auto_create_bundle')) {
        // If no bundle has been set as auto create target means that there is
        // an inconsistency in entity reference field settings.
        trigger_error(sprintf(
          "The 'Create referenced entities if they don't already exist' option is enabled but a specific destination bundle is not set. You should re-visit and fix the settings of the '%s' (%s) field.",
          $this->fieldDefinition->getLabel(),
          $this->fieldDefinition->getName()
        ), E_USER_WARNING);
      }
    }

    return $bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    return $values['target_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $error, array $form, FormStateInterface $form_state) {
    return $element;
  }

}
