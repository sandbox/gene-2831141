-- SUMMARY --

Chosen Ajax uses the Chosen jQuery plugin to make your autocomplete fields
elements more user-friendly.


-- INSTALLATION --

  1. Download the Chosen module and the Chosen jQuery plugin
     (http://harvesthq.github.io/chosen/ version 1.5 or higher is recommended)
     and extract the file under "libraries".
  2. Download and enable this module.
  3. Configure at Administer > Configuration > User interface > Chosen
     (requires administer site configuration permission)
