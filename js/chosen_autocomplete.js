/**
 * @file
 * Implements Chosen widget.
 */

(function ($) {
  Drupal.behaviors.chosen_ajax = {
    attach: function (context) {
      $('input.chosen-ajax', context).once('chosen').each(function () {
        var uri = this.value;
        var selectId = '#' + this.id.substr(0, this.id.length - 12);
        var entityReferenceId = selectId.replace(/\-select$/, '');
        var $selectBox = $(selectId);

        var autocompleteId = entityReferenceId.replace('-target-id', '-chosen-chosen-autocreate');
        var $canAutocreate = $(autocompleteId).val();
        var options = {canAutocreate: $canAutocreate};
        var ajaxChosenOptions = chosenOptions = options;
        $.extend(ajaxChosenOptions, {
          method: 'GET',
          url: uri,
          dataType: 'json',
          jsonTermKey: ''
        });
        $selectBox.ajaxChosen(ajaxChosenOptions, function (data) {
          return data;
        }, chosenOptions);
        /**
         * Handles auto complete.
         *
         * @param value
         * @param action
         */
        function entityReferenceModifyAutocomplete(value, action) {
          if (value === '') {
            return;
          }
          var $entityReference = $(entityReferenceId);
          var $entityReferenceValueText = $entityReference.val();
          var entityReferenceValueArray = $entityReferenceValueText.split(',');
          if (action === 'push') {
            entityReferenceValueArray.push(value);
          }
          else {
            entityReferenceValueArray.remove(value);
          }
          entityReferenceValueArray = entityReferenceValueArray.filter(function (elem, pos) {
            return entityReferenceValueArray.indexOf(elem) === pos;
          });
          $entityReferenceValueText = '';
          if (entityReferenceValueArray.length === 1) {
            $entityReferenceValueText = entityReferenceValueArray[0];
          }
          else if (entityReferenceValueArray.length > 1) {
            $entityReferenceValueText = entityReferenceValueArray.filter(function (elem, pos) {
              return (elem !== '');
            }).join(',')
          }
          $entityReference.val($entityReferenceValueText);
        }

        $selectBox.on('change', function (evt, params) {

          $(this).find('option').each(function (i, element) {
            if (element.selected) {
              $(selectId + ' option:eq(' + i + ')').attr('selected', 'selected');
              entityReferenceModifyAutocomplete(element.value, 'push')
            }
            else {
              $(selectId + ' option:eq(' + i + ')').removeAttr('selected');
              entityReferenceModifyAutocomplete(element.value, 'drop')
            }
          });
        });
      });
    }
  }
})(jQuery);


Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};
